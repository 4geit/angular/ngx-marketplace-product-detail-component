import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMarketplaceProductDetailComponent } from './ngx-marketplace-product-detail.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxCartButtonModule } from '@4geit/ngx-cart-button-component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    NgxCartButtonModule,
  ],
  declarations: [
    NgxMarketplaceProductDetailComponent
  ],
  exports: [
    NgxMarketplaceProductDetailComponent
  ]
})
export class NgxMarketplaceProductDetailModule { }
