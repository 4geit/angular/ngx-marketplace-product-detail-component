import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

@Component({
  selector: 'ngx-marketplace-product-detail',
  template: require('pug-loader!./ngx-marketplace-product-detail.component.pug')(),
  styleUrls: ['./ngx-marketplace-product-detail.component.scss']
})
export class NgxMarketplaceProductDetailComponent implements OnInit {

  item: any;
  private sub: any;

  constructor(
    private productsService: NgxMarketplaceProductsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.route.params
      .subscribe((params: Params) => {
        this.item = this.productsService.getProductDetail(params['slug']);
        this.item.quantity = 50;
        this.item.color = 'black';
        this.updateTotal();
      })
    ;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  updateTotal() {
    this.item.total = this.item.price * this.item.quantity;
  }

}
