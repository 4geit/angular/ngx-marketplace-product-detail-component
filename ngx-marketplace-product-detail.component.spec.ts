import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceProductDetailComponent } from './ngx-marketplace-product-detail.component';

describe('marketplace-product-detail', () => {
  let component: marketplace-product-detail;
  let fixture: ComponentFixture<marketplace-product-detail>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-product-detail ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-product-detail);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
